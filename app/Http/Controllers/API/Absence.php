<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Absence extends Controller
{
    public function listAbsence(Request $req)
    {
        try {
            if ($req->session()->has('user')) {
                // Code Add Type Formation  
                if (session()->get('user')->type == 2) {

                    $absence = DB::select('SELECT a.id, dateAbsence, p.label AS pseudo, nom, ta.label AS type 
                    FROM typeabsence ta 
                    INNER JOIN absence a ON ta.id = a.type
                    INNER JOIN candidat c ON c.pseudo = a.id
                    INNER JOIN
                     pseudo p on c.pseudo = p.id');
                    return [
                        "data" => $absence,
                        "Absence successfully imported"
                    ];
                } else {
                    $req->session()->flush();
                    return "Vous n'etes pas autorisé";
                };
            } else return "Reconnectez-vous";
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function getAbsenceType(Request $req, $id)
    {
        try {
            if ($req->session()->has('user')) {
                if (session()->get('user')->type == 2) {

                    $now = DB::select('SELECT CURDATE()');

                    $debut = DB::table('candidat')
                        ->join('formationcandidat', 'candidat.id', '=', 'formationcandidat.candidat')
                        ->join('formation', 'formationcandidat.formation', '=', 'formation.id')
                        ->where('formation.formationType', '=', $id)
                        ->select('formation.dateDebut')
                        ->distinct()->get();

                    $fin = DB::table('candidat')
                        ->join('formationcandidat', 'candidat.id', '=', 'formationcandidat.candidat')
                        ->join('formation', 'formationcandidat.formation', '=', 'formation.id')
                        ->where('formation.formationType', '=', $id)
                        ->select('formation.dateFin')
                        ->distinct()->get();

                    $absence = DB::select('SELECT c.id, c.nom, c.prenom, 
                    IF(EXISTS(select * from absence where absence.candidat=c.ID and DATE(absence.dateAbsence) = CURDATE() and type=1),true,false) as "absence matin",
                    IF(EXISTS(select * from absence where absence.candidat=c.ID and DATE(absence.dateAbsence) = CURDATE() and type=2),true,false) as "absence soir" 
                    from candidat c
                   where c.validation = ? and CURDATE() between ? and ? ', [$id, $debut[0]->dateDebut, $fin[0]->dateFin]);
                    return [
                        "absence" => $absence,
                        // $now,
                        // $debut,
                        // $fin,
                        "Absence successfully imported"
                    ];
                } else {
                    $req->session()->flush();
                    return "Vous n'etes pas autorisé";
                };
            } else return "Reconnectez-vous";
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }


}

  // $debut = DB::select('SELECT DISTINCT dateDebut from formation f ,formationcandidat fc, candidat c  where c.id =fc.candidat and fc.formation = f.id and candidat and ? = f.formationType', [$id]);
  // $fin = DB::select('SELECT DISTINCT dateFin from formation f ,formationcandidat fc, candidat c  where c.id =fc.candidat and fc.formation = f.id and  ? = f.formationType', [$id]);