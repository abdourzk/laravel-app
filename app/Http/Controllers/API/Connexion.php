<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Connexion extends Controller
{
    public function login(Request $req)
    {
        try {
            $user = DB::table('users')->where([
                ['pseudo', '=', $req->Pseudo],
                ['password', '=', $req->Password]
            ])->first();
            $req->session()->regenerate();
            if ($user) $req->session()->put(['user' => $user]);

            return [
                "etat" => 1,
                "data" => $user,
                "sessionId" => $req->session()->getId(),
            ];
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function logout(Request $req)
    {
        try {
            $id = $req->session()->getId();

            $req->session()->flush();
            return ["sessionId" => $id];
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}
